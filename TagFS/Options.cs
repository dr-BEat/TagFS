﻿using System.Collections.Generic;
using CommandLine;

namespace TagFS
{
    class CommonOptions
    {
        [Option('v', "verbose", Required = false, HelpText = "Prints all messages to standard output.")]
        bool Verbose { get; set; }
    }

    [Verb("tag", HelpText = "Tag files.")]
    class TagOptions : CommonOptions
    {
        [Value(0, Required = true)]
        public string FilePattern { get; set; }

        [Value(1, Min = 1, Required = true)]
        public IEnumerable<string> Tags { get; set; }
    }

    [Verb("tags", HelpText = "Lists all tags.")]
    class TagsOptions : CommonOptions
    {
        [Value(0, Required = false)]
        public string FilePattern { get; set; }
    }

    [Verb("files", HelpText = "Lists all files.")]
    class FilesOptions : CommonOptions
    {
        [Value(0, Min = 1, Required = true)]
        public IEnumerable<string> TagPatterns { get; set; }
    }

    [Verb("demo", HelpText = "Demo Database.")]
    class DemoOptions : CommonOptions
    {
    }
}
