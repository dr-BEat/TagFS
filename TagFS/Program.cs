﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommandLine;
using LiteDB;
using TagFS.Entities;

namespace TagFS
{
    public class Program
    {
        public static int Main(string[] args)
        {
            return Parser.Default.ParseArguments<TagOptions, TagsOptions, FilesOptions, DemoOptions>(args)
                  .MapResult(DbWrapper<TagOptions>(Tag),
                    DbWrapper<TagsOptions>(Tags),
                    DbWrapper<FilesOptions>(Files),
                    DbWrapper<DemoOptions>(Demo),
                    _ => 1);
        }

        private static Func<T, int> DbWrapper<T>(Func<Database, T, int> action) where T : CommonOptions
        {
            return options => DbWrapper(action, options);
        }

        private static int DbWrapper<T>(Func<Database, T, int> action, T options) where T : CommonOptions
        {
            using (var db = new Database())
            {
                return action(db, options);
            }
        }

        private static int Tag(Database db, TagOptions options)
        {
            return 0;
        }

        private static int Tags(Database db, TagsOptions options)
        {
            foreach (var tagGroup in db.Tags.FindAll().GroupBy(t => t.Name))
            {
                Console.WriteLine("{0} values: {1}", tagGroup.Key, string.Join(", ", tagGroup.Select(t => t.Value)));
            }

            return 0;
        }
        
        private static int Files(Database db, FilesOptions options)
        {
            var tagNames = new HashSet<string>(options.TagPatterns);
            
            // TODO: Implement a query that runs in the database instead of post filtering.
            foreach (var file in db.Files.IncludeAll().FindAll().Where(f => tagNames.IsSubsetOf(f.Tags.Select(t => t.Name))))
            {
                Console.WriteLine("{0} id: {1} tags: {2}", file.Name, file.FileId,
                    string.Join(", ", file.Tags));
            }

            return 0;
        }

        private static int Demo(Database db, DemoOptions options)
        {
            db.DropAllCollections();
                
            var musicTag = new Tag
            {
                Name = "music"
            };
            db.Tags.Insert(musicTag);

            var filmsTag = new Tag
            {
                Name = "films"
            };
            db.Tags.Insert(filmsTag);

            var year2010Tag = new Tag
            {
                Name = "year",
                Value = "2010"
            };
            db.Tags.Insert(year2010Tag);

            var year2012Tag = new Tag
            {
                Name = "year",
                Value = "2012"
            };
            db.Tags.Insert(year2012Tag);

            // create some files and connect them to tags
            var mp3 = new File
            {
                Name = "test.mp3",
                Tags = new List<Tag>{ musicTag, year2010Tag }
            };
            db.Files.Insert(mp3);

            var mkv = new File
            {
                Name = "robocob.mkv",
                Tags = new List<Tag> { filmsTag, year2010Tag }
            };
            db.Files.Insert(mkv);

            return 0;
        }

    }
}
