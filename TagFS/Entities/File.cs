﻿using System.Collections.Generic;
using LiteDB;

namespace TagFS.Entities
{
    public class File
    {
        public long FileId { get; set; }
        public string Name { get; set; }

        [BsonRef("tags")]
        public List<Tag> Tags { get; set; }
    }
}
