﻿using LiteDB;

namespace TagFS.Entities
{
    public class Tag
    {
        public long TagId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Value))
            {
                return Name;
            }
            return $"{Name}={Value}";
        }
    }
}
