﻿using System;
using System.Linq;
using LiteDB;

namespace TagFS.Entities
{
    public sealed class Database : IDisposable
    {
        private readonly LiteDatabase _db;
        private readonly Lazy<LiteCollection<Tag>> _tags;
        private readonly Lazy<LiteCollection<File>> _files;

        public Database()
        {
            _db = new LiteDatabase("tagfs.db");
            _tags = new Lazy<LiteCollection<Tag>>(() =>
            {
                var collection = _db.GetCollection<Tag>("tags");
                collection.EnsureIndex(x => x.Name);
                return collection;
            });
            _files = new Lazy<LiteCollection<File>>(() =>
            {
                var collection = _db.GetCollection<File>("files");
                collection.EnsureIndex(x => x.Tags[0].Name);
                return collection;
            });
        }

        public LiteCollection<Tag> Tags => _tags.Value;
        public LiteCollection<File> Files => _files.Value;

        public void DropAllCollections()
        {
            //Clear database
            foreach (var collectionName in _db.GetCollectionNames().ToList())
            {
                _db.DropCollection(collectionName);
            }
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
